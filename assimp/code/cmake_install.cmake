# Install script for directory: E:/Programming/assimp-3.2/assimp-3.2/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/Assimp")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Programming/assimp-3.2/Build/code/Debug/assimp-vc130-mtd.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Programming/assimp-3.2/Build/code/Release/assimp-vc130-mt.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Programming/assimp-3.2/Build/code/MinSizeRel/assimp-vc130-mt.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Programming/assimp-3.2/Build/code/RelWithDebInfo/assimp-vc130-mt.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "libassimp3.2.0")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Programming/assimp-3.2/Build/code/Debug/assimp-vc130-mtd.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Programming/assimp-3.2/Build/code/Release/assimp-vc130-mt.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Programming/assimp-3.2/Build/code/MinSizeRel/assimp-vc130-mt.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Programming/assimp-3.2/Build/code/RelWithDebInfo/assimp-vc130-mt.dll")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "assimp-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/anim.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/ai_assert.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/camera.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/color4.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/color4.inl"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/config.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/defs.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/cfileio.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/light.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/material.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/material.inl"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/matrix3x3.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/matrix3x3.inl"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/matrix4x4.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/matrix4x4.inl"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/mesh.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/postprocess.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/quaternion.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/quaternion.inl"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/scene.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/metadata.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/texture.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/types.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/vector2.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/vector2.inl"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/vector3.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/vector3.inl"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/version.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/cimport.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/importerdesc.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/Importer.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/DefaultLogger.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/ProgressHandler.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/IOStream.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/IOSystem.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/Logger.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/LogStream.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/NullLogger.hpp"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/cexport.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/Exporter.hpp"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "assimp-dev")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/Compiler/pushpack1.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/Compiler/poppack1.h"
    "E:/Programming/assimp-3.2/assimp-3.2/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE FILES "E:/Programming/assimp-3.2/Build/code/Debug/assimpd.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE FILES "E:/Programming/assimp-3.2/Build/code/RelWithDebInfo/assimp.pdb")
  endif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
endif()

