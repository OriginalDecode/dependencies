# CMake generated Testfile for 
# Source directory: E:/Programming/Bullet Physics/bullet3-2.83.7/test
# Build directory: E:/Programming/Bullet Physics/Builds/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(InverseDynamics)
subdirs(gtest-1.7.0)
subdirs(collision)
subdirs(BulletDynamics/pendulum)
