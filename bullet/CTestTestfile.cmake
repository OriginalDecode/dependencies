# CMake generated Testfile for 
# Source directory: E:/Programming/Bullet Physics/bullet3-2.83.7
# Build directory: E:/Programming/Bullet Physics/Builds
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(examples)
subdirs(Extras)
subdirs(src)
subdirs(test)
