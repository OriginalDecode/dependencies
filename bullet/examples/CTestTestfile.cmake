# CMake generated Testfile for 
# Source directory: E:/Programming/Bullet Physics/bullet3-2.83.7/examples
# Build directory: E:/Programming/Bullet Physics/Builds/examples
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(HelloWorld)
subdirs(BasicDemo)
subdirs(ExampleBrowser)
subdirs(ThirdPartyLibs/Gwen)
subdirs(OpenGLWindow)
