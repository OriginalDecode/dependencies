# CMake generated Testfile for 
# Source directory: E:/Programming/Bullet Physics/bullet3-2.83.7/Extras/Serialize
# Build directory: E:/Programming/Bullet Physics/Builds/Extras/Serialize
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(BulletFileLoader)
subdirs(BulletXmlWorldImporter)
subdirs(BulletWorldImporter)
