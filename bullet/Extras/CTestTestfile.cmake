# CMake generated Testfile for 
# Source directory: E:/Programming/Bullet Physics/bullet3-2.83.7/Extras
# Build directory: E:/Programming/Bullet Physics/Builds/Extras
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(Serialize)
subdirs(ConvexDecomposition)
subdirs(HACD)
subdirs(GIMPACTUtils)
subdirs(InverseDynamics)
